
import java.util.ArrayList;


/**
 *
 * TODO_javadoc.
 *
 * @version 1.0 (01/04/2022).
 *
 */
public class Cloud {


    /*******************************************************************************************************
     * Cloud
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     */
    public Cloud() {

        lamps        = new ArrayList<>();
        rollerShades = new ArrayList<>();
    }

    /*******************************************************************************************************
     * addRollerShade
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param rollerShade TODO_javadoc.
     *
     */
    public void addRollerShade(RollerShade rollerShade) {
        rollerShades.add(rollerShade);
    }

    /*******************************************************************************************************
     * addLamp
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *o
     *
     */
    public void addLamp(Lamp lamp) {
        lamps.add(lamp);
    }

    /*******************************************************************************************************
     * advanceTime
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param delta TODO_javadoc.
     *
     */
    public void advanceTime(double delta) {

        // solo a las cortinas le afecta el tiempo

        for (RollerShade rollerShade : rollerShades) {
            rollerShade.advanceTime(delta);
        }
    }


    /*******************************************************************************************************
     * startUpRollerShade
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param channel TODO_javadoc.
     *
     */
    public void startUpRollerShade(int channel) {

        for (RollerShade rollerShade : rollerShades) {

            if (rollerShade.getChannel() == channel) {
                rollerShade.startUp();
            }
        }
    }

    /*******************************************************************************************************
     * startDownRollerShade
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param channel TODO_javadoc.
     *
     */
    public void startDownRollerShade(int channel) {

        for (RollerShade rollerShade : rollerShades) {

            if (rollerShade.getChannel() == channel) {
                rollerShade.startDown();
            }
        }
    }

    /*******************************************************************************************************
     * stopRollerShade
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param channel TODO_javadoc.
     *
     */
    public void stopRollerShade(int channel) {

        for (RollerShade rollerShade : rollerShades) {

            if (rollerShade.getChannel() == channel) {
                rollerShade.stop();
            }
        }
    }

    //
    //

    /*******************************************************************************************************
     * changeLampPowerState
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param channel TODO_javadoc.
     *
     */
    public void changeLampPowerState(int channel) {

        for (Lamp lamp : lamps) {

            if (lamp.getChannel() == channel) {
                lamp.changePowerState();
            }
        }
    }

    /*******************************************************************************************************
     * upLampIntensity
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param channel TODO_javadoc.
     * @param color TODO_javadoc.
     *
     */
    public void upLampIntensity(int channel, String color) {

        for (Lamp lamp : lamps) {

            if (lamp.getChannel() == channel) {
                lamp.upIntensity(color);
            }
        }
    }

    /*******************************************************************************************************
     * downLampIntensity
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param channel TODO_javadoc.
     * @param color TODO_javadoc.
     *
     */
    public void downLampIntensity(int channel, String color) {

        for (Lamp lamp : lamps) {

            if (lamp.getChannel() == channel) {
                lamp.downIntensity(color);
            }
        }
    }

    /*******************************************************************************************************
     * getHeaders
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @return TODO_javadoc.
     *
     */
    public String getHeaders() {

        StringBuffer sb = new StringBuffer();

        for (DomoticDevice rollerShade : rollerShades) {
            sb.append(rollerShade.getHeader() + "\t");
        }

        for (DomoticDevice lamp : lamps) {
            sb.append(lamp.getHeader() + "\t");
        }

        return sb.toString();
    }
    /*******************************************************************************************************
     * getPowerState
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @return TODO_javadoc.
     *
     */
    public Lamp.LampState getPowerState(){
        Lamp.LampState power = null;
        for (Lamp l : lamps){
            power=l.getPowerState();
        }
        return power;
    }
    /*******************************************************************************************************
     * getState
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @return TODO_javadoc.
     *
     */
    public String getState() {

        StringBuffer sb = new StringBuffer();

        for (DomoticDevice rollerShade : rollerShades) {
            sb.append(rollerShade.toString() + "\t");
        }

        for (DomoticDevice lamp : lamps) {
            sb.append(lamp.toString() + "\t");
        }

        return sb.toString();
    }


    /** TODO_javadoc. */
    private ArrayList<Lamp> lamps = null;

    /** TODO_javadoc. */
    private ArrayList<RollerShade> rollerShades = null;
}
