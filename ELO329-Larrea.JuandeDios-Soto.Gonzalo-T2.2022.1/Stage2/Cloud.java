import javax.swing.plaf.nimbus.State;
import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<Lamp>();
        rollerShades = new ArrayList<RollerShade>();
    }
/***********************************************************
 *AddRollerShade
 **********************************************************/

    public void addRollerShade(RollerShade rollerShade) {
        rollerShades.add(rollerShade);
    }
    public RollerShade getRollerShadeAtChannel(int channel){
        for (RollerShade rs: rollerShades){
            if (rs.getChannel()== channel){
                return rs;
            }
        }
        return null;
    }

/***********************************************************
 *StartShadeUp
 **********************************************************/
    public void startShadeUp(int channel) {
        for (DomoticDevice domoticDevice : rollerShades){
            RollerShade rollerShade = (RollerShade) domoticDevice;
            if (domoticDevice.getChannel()==channel){
                rollerShade.startUp();
            }
        }
    }
/***********************************************************
 *StartShadeDown
 **********************************************************/
    public void startShadeDown(int channel) {
        for (DomoticDevice domoticDevice : rollerShades){
            RollerShade rollerShade = (RollerShade) domoticDevice;
            if (domoticDevice.getChannel()==channel){
                rollerShade.startDown();
            }
        }
    }

/***********************************************************
 *StopShade
 **********************************************************/
    public void stopShade(int channel) {
        for (DomoticDevice domoticDevice : rollerShades){
            RollerShade rollerShade = (RollerShade) domoticDevice;
            if (domoticDevice.getChannel()==channel){
                rollerShade.stop();
            }
        }
    }

/***********************************************************
 *
 **********************************************************/
    private ArrayList<Lamp> lamps;

    private ArrayList<RollerShade> rollerShades;
}
