import javax.swing.plaf.nimbus.State;
import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps        = new ArrayList<DomoticDevice>();
        rollerShades = new ArrayList<RollerShade>();
        lampControls = new ArrayList<DomoticDeviceControl>();
    }

/***********************************************************
*Lamp
**********************************************************/
    public void addLampControls(DomoticDeviceControl lc){
        lampControls.add(lc);
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }

    public Lamp getLampAtChannel( int channel){
        for (DomoticDevice domoticDevice: lamps)
            if (domoticDevice.getChannel() == channel)
                return (Lamp) domoticDevice;
        return null;
    }
    public ArrayList<DomoticDevice> getLamps(){
        return lamps;
    }
    public void changeLampPowerState(int channel){
        for (DomoticDevice domoticDevice : lamps){
            Lamp lamp = (Lamp) domoticDevice;
            if (domoticDevice.getChannel()==channel){
                lamp.changePowerState();
            }
        }
    }
    public LampState getLampState(int channel){
        LampState state = null;
        for (DomoticDevice domoticDevice : lamps){
            Lamp lamp =(Lamp) domoticDevice;
            if (domoticDevice.getChannel()==channel){
                state = lamp.getState();
            }
        }
        return state;
    }

    public ArrayList<DomoticDeviceControl> getLampControls(){
        return lampControls;
    }

    public void changeRGB(int channel, short r, short g, short b){
        for (DomoticDevice domoticDevice : lamps){
            Lamp lamp = (Lamp) domoticDevice;
            if (domoticDevice.getChannel()==channel){
                if (getLampState(channel)== LampState.ON){
                    lamp.changeColor(r,g,b);
                }
                else
                    lamp.changeColor((short)255,(short)255,(short) 255);
            }
        }
    }

/***********************************************************
*RollerShade
**********************************************************/
    public void addRollerShade(RollerShade rollerShade) {
        rollerShades.add(rollerShade);
    }
    public RollerShade getRollerShadeAtChannel(int channel){
        for (RollerShade rs: rollerShades){
            if (rs.getChannel()== channel){
                return rs;
            }
        }
        return null;
    }

    public void startShadeUp(int channel) {
        for (DomoticDevice domoticDevice : rollerShades){
            RollerShade rollerShade = (RollerShade) domoticDevice;
            if (domoticDevice.getChannel()==channel){
                rollerShade.startUp();
            }
        }
    }

    public void startShadeDown(int channel) {
        for (DomoticDevice domoticDevice : rollerShades){
            RollerShade rollerShade = (RollerShade) domoticDevice;
            if (domoticDevice.getChannel()==channel){
                rollerShade.startDown();
            }
        }
    }

    public void stopShade(int channel) {
        for (DomoticDevice domoticDevice : rollerShades){
            RollerShade rollerShade = (RollerShade) domoticDevice;
            if (domoticDevice.getChannel()==channel){
                rollerShade.stop();
            }
        }
    }

/***********************************************************
*
**********************************************************/

    private ArrayList<DomoticDevice> lamps;
    private ArrayList<RollerShade> rollerShades;
    private ArrayList<DomoticDeviceControl> lampControls;
}
