import javafx.scene.layout.Pane;

import java.util.ArrayList;

public class LampControl extends DomoticDeviceControl{
    public LampControl(int channel, Cloud c) {
        super(channel,c);
        cloud = c;
        view  = new LampControlView(this);
    }
    public void pressPower(int channel){
        cloud.changeLampPowerState(channel);
    }
    public LampState getLampState(int channel){
        return cloud.getLampState(channel);
    }
    public ArrayList<DomoticDeviceControl> getLampsControls(){
        return cloud.getLampControls();
    }
    public ArrayList<DomoticDevice> getCantLamps(){
        return cloud.getLamps();
    }

    public void changeRGB(int channel,short r, short g, short b){
        cloud.changeRGB(channel,r, g, b);
    }
    public Pane getView() { return view;}
    private Cloud cloud;
    private Pane view;
}
