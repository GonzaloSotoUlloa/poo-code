import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.awt.*;

public class RollerShadeView extends Group {
    public RollerShadeView(double maxLength, double width, double length, double radius, Color color) {
        Image fondo          = new Image("src/fondo.png");
        Rectangle background = new Rectangle(5,5, width-10,maxLength-5);
        background.setFill(new ImagePattern(fondo));  // I chose Blue
        getChildren().add(background);

        cloth = new Rectangle (0,1,width,length);
        cloth.setFill(color);
        cloth.setHeight(length+5);

        // Rolled up shade cloth
        Ellipse rightSide = new Ellipse(0, radius,radius/2,radius);
        rightSide.setFill(color);
        rightSide.setStroke(Color.BLACK);
        getChildren().addAll(cloth, rightSide);
    }
    public void setLength(double length) {
        cloth.setHeight(length);
    }
    private Rectangle cloth;
}
