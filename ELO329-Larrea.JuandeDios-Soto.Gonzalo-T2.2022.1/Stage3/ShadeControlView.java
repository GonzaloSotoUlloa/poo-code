import javafx.geometry.HPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ShadeControlView extends BorderPane {
    public ShadeControlView (ShadeControl sc){

        Image incremental         = new Image("src/arrow.jpg");
        ImageView incrementalView = new ImageView(incremental);

        Image down         = new Image("src/arrow.jpg");
        ImageView downView = new ImageView(down);

        Image decremental          = new Image("src/arrow.jpg");
        ImageView decrementalView  = new ImageView(decremental);

        Image up         = new Image("src/arrow.jpg");
        ImageView upView = new ImageView(up);

        Button channelButton    = new Button("0");
        Button DownRoller       = new Button();
        Button UpRoller         = new Button();
        Button incrementChannel = new Button();
        Button decrementChannel = new Button();

        incrementChannel.setGraphic(incrementalView);
        downView.setRotate(90);
        DownRoller.setGraphic(downView);

        decrementalView.setRotate(180);
        decrementChannel.setGraphic(decrementalView);

        upView.setRotate(270);
        UpRoller.setGraphic(upView);

/***********************************************************
 *Actions
 **********************************************************/

        channelButton.setOnAction( e-> {
            if (getChannelActual()==sc.getChannel()){
                sc.stop();
            }else {
                System.out.println("No roller");
            }

        });
        incrementChannel.setOnAction(e->{
            channelActual++;
            channelButton.setText(""+getChannelActual());
        });
        decrementChannel.setOnAction(e->{
            if (getChannelActual()>0){
                channelActual--;
                channelButton.setText(""+getChannelActual());
            }
        });

        UpRoller.setOnAction(e->{
            if (sc.getChannel()==getChannelActual()){
                sc.startUp();
            }
        });
        DownRoller.setOnAction(e->{
            if (sc.getChannel()== getChannelActual()){
                sc.startDown();
            }
        });

/***********************************************************
 *
 **********************************************************/

        GridPane GP = new GridPane();
        GP.add(UpRoller,1,0);
        GP.add(channelButton,1,1);
        GP.setHalignment(channelButton, HPos.CENTER);
        GP.add(DownRoller,1,2);
        GP.add(decrementChannel,0,1);
        GP.add(incrementChannel,2,1);
        setCenter(GP);
    }
    public int getChannelActual(){
        return channelActual;
    }
    int channelActual=0;
}
