import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Stage3 extends Application {
    public void start(Stage primaryStage) {
        int channelLamp1    = 2;
        int channelLamp2    = 3;
        Cloud cloud         = new Cloud();
        DomoticDevice lamp1 = new Lamp(channelLamp1);
        DomoticDevice lamp2 = new Lamp(channelLamp2);
        DomoticDeviceControl lampControl = new LampControl(0,cloud);
        cloud.addLamp((Lamp) lamp1);
        cloud.addLamp((Lamp) lamp2);
        cloud.addLampControls(lampControl);
        System.out.println("Lamp 1 Channel: " + lamp1.getChannel());
        System.out.println("Lamp 2 Channel: " + lamp2.getChannel());
        HBox hBox = new HBox(20);
        hBox.setPadding(new Insets(20));
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().addAll(((Lamp) lamp1).getView(),((Lamp) lamp2).getView(), ((LampControl) lampControl).getView());

        BorderPane pane = new BorderPane();
        pane.setPadding(new Insets(20));
        pane.setBottom(hBox);
        int shadeChannel = 1;

        RollerShade rs = new RollerShade(shadeChannel, 2, 150, 100);
        cloud.addRollerShade(rs);
        System.out.println("RollerShade 1 Channel: " + rs.getChannel());
        pane.setLeft(rs.getView());
        ShadeControl shadeControl = new ShadeControl(shadeChannel,cloud);
        hBox.getChildren().add(0,shadeControl.getView());
        Scene scene = new Scene(pane, 500, 500);
        primaryStage.setTitle("Domotic Devices Simulator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
