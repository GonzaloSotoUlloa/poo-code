import javafx.scene.Node;

public class Lamp extends DomoticDevice{
    public Lamp(int channel) {
        super(channel);
        r=g=b = 255;
        state = LampState.OFF;
        view  = new LampView();
    }

    public void changePowerState(){
        state = state==LampState.ON ? LampState.OFF : LampState.ON;
        if (state==LampState.OFF) view.setColor((short)0,(short)0, (short)0);
        else view.setColor(r,g,b);
    }
    public LampState getState(){
        return state;
    }
    public void changeColor(short r, short g, short b){
        this.r = r;
        this.g = g;
        this.b = b;
        view.setColor(r, g, b);
    }
    public short getR(){
        return r;
    }
    public short getG(){
        return g;
    }
    public short getB(){
        return b;
    }
    public Node getView() {
        return view;
    }
    private short r,g,b;
    private LampState state;
    private LampView view;
}
