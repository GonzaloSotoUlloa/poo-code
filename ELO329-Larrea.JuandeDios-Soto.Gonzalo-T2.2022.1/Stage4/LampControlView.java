import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;

public class LampControlView extends VBox{
    public LampControlView(LampControl lampControl) {

        VBox vbox       = new VBox();
        Spinner spinner = new Spinner(0, 5, 0);

        spinner.setOnMouseClicked(e -> {
            int canalActual = (Integer) spinner.getValue();
            lampControl.setChannel(canalActual);
        });

        vbox.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.SPACE) {
                if (lampControl.getLampState(lampControl.getChannel()) == LampState.ON) {
                        lampControl.pressPower(lampControl.getChannel());
                        lampControl.changeRGB(lampControl.getChannel(), (short) 255, (short) 255, (short) 255);
                        System.out.println("Lamp State: " + lampControl.getLampState(lampControl.getChannel()));
                    } else {
                        lampControl.pressPower(lampControl.getChannel());
                        lampControl.changeRGB(lampControl.getChannel(), getR(), getG(), getB());
                    System.out.println("Lamp State: " + lampControl.getLampState(lampControl.getChannel()));
                    }
                }
        });
        Image img          = new Image("src/powerImage.jpg");
        ImageView view     = new ImageView(img);
        Button buttonOnOFF = new Button();
        buttonOnOFF.setGraphic(view);
        buttonOnOFF.setOnAction(actionEvent -> {
                    if (lampControl.getLampState(lampControl.getChannel()) == LampState.ON) {
                        lampControl.pressPower(lampControl.getChannel());
                        lampControl.changeRGB(lampControl.getChannel(), (short) 255, (short) 255, (short) 255);
                        System.out.println("Lamp State: " + lampControl.getLampState(lampControl.getChannel()));
                    } else if (lampControl.getLampState(lampControl.getChannel())==LampState.OFF){
                        lampControl.pressPower(lampControl.getChannel());
                        lampControl.changeRGB(lampControl.getChannel(), getR(), getG(), getB());
                        System.out.println("Lamp State: " + lampControl.getLampState(lampControl.getChannel()));
                    }
                    else
                        System.out.println("No hay lampara en este canal: " + lampControl.getChannel());

            }
        );

/***********************************************************
 *Slider
 **********************************************************/

        Slider sliderR = new Slider(0, 255, 255);
        sliderR.setShowTickLabels(true);
        sliderR.setMajorTickUnit(50);
        sliderR.valueProperty().addListener((observableValue, number, t1) -> {
             if (lampControl.getLampState(lampControl.getChannel()) == LampState.ON) {
                 r = t1.shortValue();
                 labelRed.setText(String.valueOf("Red: " + t1.shortValue()));
                 lampControl.changeRGB(lampControl.getChannel(), r, g, b);
             }
        });
        Slider sliderG = new Slider(0, 255, 255);
        sliderG.setShowTickLabels(true);
        sliderG.setMajorTickUnit(50);
        sliderG.valueProperty().addListener((observableValue, number, t1) -> {

                if (lampControl.getLampState(lampControl.getChannel())==LampState.ON){
                    g = t1.shortValue();
                    labelGreen.setText(String.valueOf("Green: " + t1.shortValue()));
                    lampControl.changeRGB(lampControl.getChannel(),r,g,b);
                }

        });
        Slider sliderB = new Slider(0, 255, 255);
        sliderB.setShowTickLabels(true);
        sliderB.setMajorTickUnit(50);
        sliderB.valueProperty().addListener((observableValue, number, t1) -> {

                if (lampControl.getLampState(lampControl.getChannel()) == LampState.ON) {
                    b = t1.shortValue();
                    labelBlue.setText(String.valueOf("Blue: " + t1.shortValue()));
                    lampControl.changeRGB(lampControl.getChannel(), r, g, b);
                }
        });
        setMargin(buttonOnOFF, new Insets(20, 20, 20, 20));
        setMargin(spinner, new Insets(10, 10, 10, 10));
        setMargin(sliderR, new Insets(10, 10, 10, 10));
        setMargin(sliderG, new Insets(10, 10, 10, 10));
        setMargin(sliderB, new Insets(10, 10, 10, 10));
        vbox.getChildren().addAll(buttonOnOFF, spinner,labelRed ,sliderR,labelGreen, sliderG, labelBlue, sliderB);
        getChildren().addAll(vbox);
    }

/***********************************************************
*
**********************************************************/

    public short getR(){
        return r;
    }

    public short getB() {
        return b;
    }

    public short getG() {
        return g;
    }
    private Label labelRed = new Label("Red: 255");
    private Label labelGreen = new Label("Green: 255");
    private Label labelBlue = new Label("Blue: 255");
    private short r=255,g=255,b=255;

}