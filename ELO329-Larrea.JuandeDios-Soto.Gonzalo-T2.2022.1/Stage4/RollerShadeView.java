import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;

import java.io.File;

public class RollerShadeView extends Group {
    public RollerShadeView(int video,double maxLength, double width, double length, double radius, Color color) {
        File file;
        File file2;

        Media media;
        Media media2;

        MediaPlayer  mediaPlayer;
        MediaPlayer  mediaPlayer2;

        MediaView mediaView  = new MediaView();
        MediaView mediaView2 = new MediaView();

        file  = new File("src/video1.mp4");
        file2 = new File("src/video2.mp4");

        media  = new Media(file.toURI().toString());
        media2 = new Media(file2.toURI().toString());

        mediaPlayer  = new MediaPlayer(media);
        mediaPlayer2 = new MediaPlayer(media2);

        mediaPlayer.setAutoPlay(true);
        mediaPlayer.setAutoPlay(true);

        mediaView.setMediaPlayer(mediaPlayer);
        mediaView2.setMediaPlayer(mediaPlayer2);
        Pane mvPane = new Pane();
        if (video == 1){
            mvPane.getChildren().addAll(mediaView);
        }else if (video == 2){
            mvPane.getChildren().add(mediaView2);
        }
        getChildren().add(mvPane);

        cloth = new Rectangle(0, 1, width, length);
        cloth.setFill(color);
        cloth.setHeight(length + 5);

        // Rolled up shade cloth
        Ellipse rightSide = new Ellipse(0, radius, radius / 2, radius);
        rightSide.setFill(color);
        rightSide.setStroke(Color.BLACK);
        getChildren().addAll(cloth, rightSide);
    }
    public void setLength(double length) {
        cloth.setHeight(length);
    }
    private Rectangle cloth;
}
