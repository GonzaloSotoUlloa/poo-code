import javafx.scene.layout.BorderPane;

public class ShadeControl extends DomoticDeviceControl{
    public ShadeControl(int channel, Cloud c){
        super(channel,c);
        view = new ShadeControlView(this);
    }
    public BorderPane getView() {return view;}

    public void startUp(int channel){
        cloud.startShadeUp(channel);
    }
    public void startDown(int channel){
        cloud.startShadeDown(channel);
    }
    public void stop(int channel){
        cloud.stopShade(channel);
    }
    private ShadeControlView view;
}
