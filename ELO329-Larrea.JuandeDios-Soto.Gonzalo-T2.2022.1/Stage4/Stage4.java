import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Stage4 extends Application {
    public void start(Stage primaryStage) {
        int channelLamp1    = 1;
        int channelLamp2    = 2;
        Cloud cloud         = new Cloud();
        DomoticDevice lamp1 = new Lamp(channelLamp1);
        DomoticDevice lamp2 = new Lamp(channelLamp2);
        DomoticDeviceControl lampControl = new LampControl(0,cloud);
        cloud.addLamp((Lamp) lamp1);
        cloud.addLamp((Lamp) lamp2);
        cloud.addLampControls(lampControl);
        System.out.println("Sus Lamparas estan en el canal: "+channelLamp1 +" "+ channelLamp2);
        HBox hBox = new HBox(20);
        hBox.setPadding(new Insets(20));
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().addAll(((Lamp) lamp1).getView(),((Lamp) lamp2).getView(), ((LampControl) lampControl).getView());

        BorderPane pane = new BorderPane();
        pane.setPadding(new Insets(20));
        pane.setBottom(hBox);
        int shadeChannel  = 1;
        int shadeChannel2 = 2;

        RollerShade rs  = new RollerShade(shadeChannel ,2, 325, 320);
        RollerShade rs2 = new RollerShade(shadeChannel2,2, 570, 320);
        System.out.println("Sus RollerShade estan en el canal: "+shadeChannel +" "+ shadeChannel2);

        cloud.addRollerShade(rs);
        cloud.addRollerShade(rs2);

        pane.setLeft(rs.getView());
        pane.setRight(rs2.getView());
        ShadeControl shadeControl = new ShadeControl(0,cloud);
        hBox.getChildren().add(0,shadeControl.getView());
        Scene scene = new Scene(pane, 1000, 800);
        primaryStage.setTitle("Domotic Devices Simulator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
