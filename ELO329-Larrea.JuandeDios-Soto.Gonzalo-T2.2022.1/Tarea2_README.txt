Archivos que componen la Tarea2 - POO: 

Cloud DomoticDevice DomoticDeviceControl Lamp LampControl LampControlView LampState LampView RollerShade RollerShadeView ShadeControl ShadeControlView Stage1-4 MAKEFILE   


El programa practica la orientación a objetos en un sistema automatizado (Domótico) con una o más cortinas motorizadas y un tipo de lámpara.
Al ser ejecutado, la salida de cada Stage muestra el comportamiento respectivo de movimiento de las cortinas y la intensidad de las lámparas dependiendo de su estado. 

Para ejectuar los MAKEFILE, los pasos a seguir son:

Terminal -> make -> make run 

En caso de no ejecutarse correctamente, puede ser necesario modificar el ROUTEFX a la ubicacion donde se encuentre la lib de javaFX.


Autores: Gonzalo Soto        201930532-8
         Juan de Dios Larrea 201930512-3


